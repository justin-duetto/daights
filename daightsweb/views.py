import datetime
import json
import time

from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.http import HttpResponse

from .models import Event, Transaction
from .forms import EventForm, PricingForm
from . import pricing
from . import reports

# Create your views here.

def index(request):
    evs = Event.objects.all().order_by('start_date')
    return render(request, 'index.html', {'events': evs})


def event_view(request, event_id):
    ev = get_object_or_404(Event, id=event_id)
    trans = Transaction.objects.filter(event=ev)
    booking_data = reports.ticket_book_curve(ev)

    f = PricingForm(request.GET, initial={'as_of_days': 14})
    ids = request.GET.getlist('event_list')
    if ids:
        events = Event.objects.filter(id__in=ids)
    else:
        events = Event.objects.exclude(name__in=['Adele', 'Beyonce'])

    as_of_days = int(f.data.get('as_of_days', 14))
    pricing_extrapolate = pricing.extrapolate_data(ev, events, as_of_days)
    average_price = reports.get_average_price(events)
    error, total_error = pricing.price_recommendations(ev, events, as_of_days)

    # Fake recommended price
    import math
    recommended_price = average_price
    for data in total_error:
        if data[0] == as_of_days:
            if data[1] > 0:
                recommended_price = average_price * 1.05
            else:
                recommended_price = average_price * .95

    item_pt_segment = reports.ticket_segment_book_curve(ev, 'item_pt')
    
    # Lost Business
    regrets, denials = reports.get_lost_business(ev)
    # lost_business = reports.get_lost_business(regret_denials)

    # Seating
    seating = reports.get_seat_prices(ev)

    return render(request, 'event.html',
                  {'event': ev, 'transactions': trans, 'curve_data': booking_data,
                      'pricing': pricing_extrapolate, 'event_form': f,
                      'average_price': average_price, 'recommended_price': recommended_price,
                      'error_data': error, 'total_error_data': total_error, 'item_pt': item_pt_segment,
                      'regrets_data': regrets, 'denials_data': denials,
                      'seating': seating})


def all_events(request):
    f = EventForm(request.GET)
    ids = request.GET.getlist('event_list')
    if ids:
        events = Event.objects.filter(id__in=ids)
    else:
        events = Event.objects.exclude(name__in=['Adele', 'Beyonce'])
    data, exp_data = reports.get_average_book_curves(events)
    return render(request, 'all_events.html', {'events': events, 'curve_data': data, 'exp_data': exp_data, 'form': f})


def calendar_events(request):
    evs = Event.objects.all()
    ret = {
        'success': 1,
        'result': [ev.calendar_serialize() for ev in evs]
    }
    return HttpResponse(json.dumps(ret), content_type='application/json')
