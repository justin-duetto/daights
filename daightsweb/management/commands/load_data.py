from datetime import datetime
from daightsweb.models import Event
from daightsweb.models import Transaction
from daightsweb.models import RegretDenial
from django.db import connection
from django.db import IntegrityError
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from nameparser import HumanName

import csv
import glob

class Command(BaseCommand):
    help = 'help me help you'
    
    TX_COLUMN_MAP = {
        'Account Number': 'account_number',
        'CustomerName': 'customer_name',
        'Item': 'item',
        'ItemName': 'item_name',
        'Qty': 'quantity',
        'ItemPL': 'item_pl',
        'ItemPT': 'item_pt',
        'Price': 'price',
        'ItemDate': 'item_date',
        # 'OriginalSalecode': 'original_sale_code',
        # 'Lead Time': 'lead_time',
        'Total Sale': 'total_sale'
    }

    RD_COLUMN_MAP = {
        'Qty': 'quantity',
        'ItemPl': 'item_pl',
        'ItemPT': 'item_pt',
        'Price': 'price',
        'SearchDate': 'search_date',
        'Outcome': 'outcome'
    }

    def create_event(self, directory, filename, event_type, event_type_name=''):
        parsed_name = filename.split('_')
        event_name = parsed_name[0].replace(directory, '')
        event_date = parsed_name[1].split('.')[0]

        event_dict = {
            'name': event_name,
            'start_date': event_date,
            'event_day_length': 1,
            'event_type': event_type,
            'linked_event_type_name': event_type_name
        }
        e = Event(**event_dict)
        e.save()

        print '~~~~ Created event: ' + event_name + ' ' + event_date
        return e

    def create_transaction(self, event, filename):
        with open(filename, 'rbU') as tsvin:
            tsvin = csv.DictReader(tsvin, delimiter='\t')
      
            for data_dict in tsvin:
                new_dict = {}
                for k, v in data_dict.items():
                    new_key = Command.TX_COLUMN_MAP.get(k)
                    if new_key:
                        new_dict[new_key] = v
        
                # Clean some values
                old_date = new_dict['item_date']
                new_date = datetime.strptime(old_date, '%m/%d/%y')
                total_sale = int(new_dict['quantity']) * float(new_dict['price'])

                human_name = HumanName(new_dict['customer_name'])
                human_name.capitalize()
                
                new_dict['customer_name'] = str(human_name)
                new_dict['item_date'] = new_date.date().isoformat()
                new_dict['event'] = event
                new_dict['total_sale'] = total_sale
                
                # Create the transaction
                try:
                    t = Transaction(**new_dict)
                    t.save()
                except IntegrityError:
                    print 'Tx could not be saved: ' + t.account_number + ' ' + t.customer_name

            print 'Finished loading transactions for event'

            """
            import pdb
            pdb.set_trace()

            first_line = tsvin.next()
            for col in first_line:

                column_names.append(col)
            
            for row in tsvin:
                print row
            
            # setattr
            

            e = Event(**d)
            """

    def create_regret_denial(self, event, filename):
        with open(filename, 'rbU') as tsvin:
            tsvin = csv.DictReader(tsvin, delimiter='\t')
            for data_dict in tsvin:
                new_dict = {}
                for k, v in data_dict.items():
                    new_key = Command.RD_COLUMN_MAP.get(k)
                    if new_key:
                        new_dict[new_key] = v
                
                # Clean some values
                old_date = new_dict['search_date']
                new_date = datetime.strptime(old_date, '%m/%d/%y')
                old_price = new_dict['price']

                if old_price.strip():
                    new_price = float(old_price)
                else:
                    new_price = 0.0
                    
                new_dict['price'] = new_price

                new_dict['search_date'] = new_date.date().isoformat()
                new_dict['event'] = event


                try:
                    rd = RegretDenial(**new_dict)
                    rd.save()
                except IntegrityError:
                    print 'RegretDenial could not be saved', new_dict

    def handle(self, *args, **options):
        # Clear existing data
        with connection.cursor() as cursor:
            cursor.execute('DELETE from daightsweb_event;')
            cursor.execute('DELETE from daightsweb_transaction;')
            cursor.execute('DELETE from daightsweb_regretdenial;')

        # Load basketball files
        data_directory = settings.BASE_DIR + '/daightsweb/data/unlvbasketball/'
        filenames = glob.glob(data_directory + '*.tsv')
        for filename in filenames:
            # Create an event from the filename
            event = self.create_event(data_directory, filename, 'L', 'UNLV NCAA Basketball')

            # Create the transactions from the file 
            self.create_transaction(event, filename)

        """
        # Load music files
        data_directory = settings.BASE_DIR + '/daightsweb/data/music/'
        filenames = glob.glob(data_directory + '*.tsv')
        for filename in filenames:
            event = self.create_event(data_directory, filename, 'E')
            self.create_transaction(event, filename)

        # Load regret and denial for Beyonce
        data_directory = settings.BASE_DIR + '/daightsweb/data/regretdenial/'
        filename = glob.glob(data_directory + '*.tsv')[0]
        event = Event.objects.get(name='Beyonce')
        self.create_regret_denial(event, filename)
        """

