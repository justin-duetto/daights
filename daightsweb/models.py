from __future__ import unicode_literals

import json

from django.db import models
from django.urls import reverse

# Create your models here.

class Event(models.Model):
    EVENT_TYPES = (
            ('N', 'Note'),
            ('E', 'Event'),
            ('L', 'LinkedEvent'),
    )

    name = models.CharField(max_length=30)
    description = models.CharField(max_length=30, null=True, blank=True)
    start_date = models.DateField()
    event_day_length = models.IntegerField()
    event_type = models.CharField(max_length=1, choices=EVENT_TYPES)
    linked_event_type_name = models.CharField(max_length=30, null=True, blank=True)

    def __str__(self):
        return '%s %s' % (self.name, self.start_date)

    def calendar_serialize(self):
        start = int(self.start_date.strftime('%s')) * 1000
        end = start + 1000
        return {
            "id": self.id,
            "title": self.name,
            "url": reverse('event_view', args=(self.id, )),
            "class": "",
            "start": start,
            "end": end
        }

class Transaction(models.Model):
    account_number = models.IntegerField()
    event = models.ForeignKey(Event, on_delete=models.PROTECT)
    customer_name = models.CharField(max_length=100)
    item = models.CharField(max_length=30)
    item_name = models.CharField(max_length=30)
    quantity = models.IntegerField()
    item_pl = models.CharField(max_length=30)
    item_pt = models.CharField(max_length=30)
    price = models.DecimalField(max_digits=12, decimal_places=2)
    total_sale = models.DecimalField(max_digits=14, decimal_places=2)
    item_date = models.DateField()
    original_sale_code = models.CharField(max_length=10)
    lead_time = models.IntegerField(null=True, blank=True)

class RegretDenial(models.Model):
    event = models.ForeignKey(Event, on_delete=models.PROTECT)
    quantity = models.IntegerField()
    item_pl = models.CharField(max_length=30, null=True, blank=True)
    item_pt = models.CharField(max_length=30, null=True, blank=True)
    price = models.DecimalField(max_digits=12, decimal_places=2, null=True)
    search_date = models.DateField()
    outcome = models.CharField(max_length=30)

