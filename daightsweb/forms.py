from django import forms

from .models import Event

class EventForm(forms.Form):
    event_list = forms.ModelMultipleChoiceField(queryset=Event.objects.all(),
            widget=forms.SelectMultiple, required=False)

class PricingForm(forms.Form):
    event_list = forms.ModelMultipleChoiceField(queryset=Event.objects.all(),
            widget=forms.SelectMultiple, required=False)
    as_of_days = forms.IntegerField(required=False)
