from . import reports

def extrapolate_data(event, events, as_of_max=10):
    # training set
    train_data, exp_data = reports.get_average_book_curves(events)
    func = reports.get_training_curve_func(train_data)
    # existing event data
    book_curve = reports.ticket_book_curve(event)
    # trim as_of_max
    trimmed_data = []
    for data in book_curve:
        if data[0] >= as_of_max:
            trimmed_data.append(data)

    extrapolated_data = []
    i = 0
    while i < as_of_max:
        extrapolated_data.insert(i, (i, func(i), 0))
        i += 1
    return trimmed_data, extrapolated_data


def price_recommendations(event, events, as_of_max=0):
    # training set
    train_data, exp_data = reports.get_average_book_curves(events)
    func = reports.get_training_curve_func(train_data)
    # existing event data
    book_curve = reports.ticket_book_curve(event)

    error_curve = []
    for data in book_curve:
        expected_value = func(data[0])
        error = data[1] - expected_value
        error_curve.append((data[0], error))

    # Integrate from the end of the error_curve
    total_error = []
    error_sum = 0
    for data in reversed(error_curve):
        error_sum += data[1]
        total_error.append((data[0], error_sum))
    total_error.reverse()

    return error_curve, total_error
