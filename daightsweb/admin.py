from django.contrib import admin

from .models import Event
from .models import Transaction
from .models import RegretDenial

# Register your models here.

class EventAdmin(admin.ModelAdmin):
	list_display = ('name', 'description', 'start_date', 'event_day_length', 'event_type')
admin.site.register(Event, EventAdmin)

class TransactionAdmin(admin.ModelAdmin):
    list_display = ('account_number', 'event', 'customer_name')
admin.site.register(Transaction, TransactionAdmin)

class RegretDenialAdmin(admin.ModelAdmin):
    list_display = ('event', 'quantity', 'item_pl', 'item_pt', 'price')
admin.site.register(RegretDenial, RegretDenialAdmin)
