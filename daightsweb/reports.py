import collections
import math
import numpy as np

from django.db import connection
from pylab import *
from scipy.optimize import curve_fit

def ticket_book_curve(event):
    # Get number of tickets sold for each day out
    with connection.cursor() as cursor:
        cursor.execute('''
            SELECT julianday(%s) - julianday(item_date) as delta, SUM(quantity), SUM(total_sale)
                FROM daightsweb_transaction
                WHERE event_id = %s
                GROUP BY delta
                ORDER BY delta
        ''', [event.start_date.isoformat(), event.id])
        rows = cursor.fetchall()

    # Perform cumulative sum
    new_rows = []
    total_tickets = 0
    total_sales = 0
    for delta, tickets, sales  in reversed(rows):
        # Insert new data
        total_tickets += tickets
        total_sales += sales
        new_rows.append((delta, total_tickets, total_sales))
    new_rows.reverse()

    return new_rows


def ticket_segment_book_curve(event, segment_name):
    # Get number of tickets sold for each day out
    with connection.cursor() as cursor:
        cursor.execute('''
            SELECT julianday(%s) - julianday(item_date) as delta, {}, SUM(quantity), SUM(total_sale)
                FROM daightsweb_transaction
                WHERE event_id = %s
                GROUP BY delta, {}
                ORDER BY delta, {}
        '''.format(segment_name, segment_name, segment_name), [event.start_date.isoformat(), event.id])
        rows = cursor.fetchall()

    # Perform cumulative sum
    segment_dict = collections.defaultdict(list)
    for delta, segment, tickets, sales in rows:
        segment_dict[segment].append((delta, tickets, sales))

    summed_segment_dict = {}

    max_delta = max([d[0] for d in rows])
    for segment, rows in segment_dict.iteritems():
        # Perform cumulative sum
        new_rows = []
        total_tickets = 0
        total_sales = 0
        for delta, tickets, sales in reversed(rows):
            # Insert new data
            total_tickets += tickets
            total_sales += sales
            new_rows.append((delta, total_tickets, total_sales))
        new_rows.reverse()
        summed_segment_dict[segment] = new_rows

    return summed_segment_dict


def get_average_price(events):
    ids = [e.id for e in events]
    with connection.cursor() as cursor:
        cursor.execute('''
            SELECT avg(price) FROM daightsweb_transaction
            WHERE event_id IN (%s)
        ''' % ','.join(map(str, ids)))
        return cursor.fetchone()[0]


def get_seat_prices(event):
    with connection.cursor() as cursor:
        cursor.execute('''
            SELECT item_pl, avg(price) as avg
            FROM daightsweb_transaction
            WHERE event_id = %s
            GROUP BY item_pl
            ORDER BY avg DESC
        ''', (event.id, ))
        return cursor.fetchall()

def get_lost_business(event):
    # Process regrets
    with connection.cursor() as cursor:
        cursor.execute('''
            SELECT julianday(%s) - julianday(search_date) as delta, SUM(quantity)
                FROM daightsweb_regretdenial
                WHERE event_id = %s AND outcome = %s
                GROUP BY delta
                ORDER by delta
        ''', [event.start_date.isoformat(), event.id, 'Regret'])
        regret_rows = cursor.fetchall()
    
    regret_sum_rows = []
    total_regret_tickets = 0
    prev = None
    for delta, tickets in reversed(regret_rows):
        total_regret_tickets += tickets
        regret_sum_rows.append((delta, total_regret_tickets))
    regret_sum_rows.reverse()

    # Process denials
    with connection.cursor() as cursor:
        cursor.execute('''
            SELECT julianday(%s) - julianday(search_date) as delta, SUM(quantity)
                FROM daightsweb_regretdenial
                WHERE event_id = %s AND outcome = %s
                GROUP BY delta
                ORDER by delta
        ''', [event.start_date.isoformat(), event.id, 'Denial'])
        denial_rows = cursor.fetchall()

    denial_sum_rows = []
    total_denials = 0
    for delta, denials in reversed(denial_rows):
        total_denials += denials
        denial_sum_rows.append((delta, total_denials))
    denial_sum_rows.reverse()

    return regret_sum_rows, denial_sum_rows
    





def avg(args):
    if args:
        return float(sum(args)) / len(args)
    return None

def exp_func(x, a, b, c):
    return a * np.exp(-b*x) + c

def get_training_curve_func(avg_data):
    """
    Takes in the plotting data from a set of curves.
    """
    exp_data = []
    x = np.array([d[0] for d in avg_data])
    y = np.array([d[1] for d in avg_data])
    popt, pcov = curve_fit(exp_func, x, y)

    def training_curve_func(blah):
        return popt[0] * np.exp(-popt[1]*blah) + popt[2]
    
    return training_curve_func


def get_average_book_curves(events):
    curves = [ticket_book_curve(ev) for ev in events]

    # Get min/max days
    x_values = set()

    for curve in curves:
        for row in curve:
            x_values.add(row[0])

    min_x = int(min(x_values))
    max_x = int(max(x_values))

    data_dict = collections.defaultdict(list)
    for curve_data in curves:
        for data in curve_data:
            data_dict[data[0]].append(data)

    out_data = []
    for x, data in data_dict.iteritems():
        if x >= 0:
            out_data.append((x, avg([d[1] for d in data]), avg([d[2] for d in data])))

    out_data.sort()

    # Exp function
    training_curve_func = get_training_curve_func(out_data)
    x = np.array([d[0] for d in out_data])
    yy = training_curve_func(x)
    exp_data = []
    for a, b in zip(x, yy):
        exp_data.append((a, b))

    return out_data, exp_data

