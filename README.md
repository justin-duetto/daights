# Duncan and the Straights?


Get started:

```shell
# checkout the code somewhere, change into directory
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt

# This will create a sqlite db file
./manage.py migrate

# Add an admin user
./manage.py createsuperuser

# Run the http server
./manage.py runserver

```